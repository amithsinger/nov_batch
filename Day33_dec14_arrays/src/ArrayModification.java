
public class ArrayModification {

	public static void main(String[] args) {

		int[] arr = { 10, 20, 30, 40, 50, 60 };

		// modifying the elements in the array
		for (int i = 0; i <= arr.length - 1; i++) {
			arr[i] = arr[i] / 2;
		}

		System.out.println("Priting elements of an array");
		for (int i : arr) {
			System.out.println(i);
		}

	}

}
