Method Overloading: Writing more than one method in a class with the same
name.

Rules: there must be change in the parameter list
i. Either in the no. of parameters
ii. Or in the datatype of parameters
iii. Or in the sequence of parameters

Return type can same or it can be different


ex: 

	int add(int a, int b)

	int add(int a, int b, int c)

	void add(int a, int b, int c, int d)
	
	double add(double a, double b)