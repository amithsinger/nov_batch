import java.util.ArrayList;

public class SampleMethodsOfCollection {
	public static void main(String[] args) {
		ArrayList list = new ArrayList();
		list.add(34);
		list.add("hello");
		list.add(4.5);
		list.add(true);
		list.add(null);
		list.add(34);
		System.out.println(list);
		System.out.println(list.size()); // 6
		System.out.println(list.isEmpty()); // false
		System.out.println(list.contains(4.5)); // true
		list.remove("hello");
		System.out.println(list);
		list.clear();
		System.out.println(list);
	}
}
