import java.util.ArrayList;
import java.util.Arrays;

public class SampleAsListExample {
	public static void main(String[] args) {
		// using Arrays.asList() method to add all the elements into collections
		ArrayList<Object> objectsList = new ArrayList<>(Arrays.asList(10, "Hello", 4.5, null, null, 4.5));
		System.out.println(objectsList);
		System.out.println(objectsList.size());

		// using for each method and lambda expression to print elements in collection
		objectsList.forEach((e) -> System.out.println(e));
	}

}
