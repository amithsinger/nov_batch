package tokenexample;

public class Token {
	int tokenNumber;

	// constructor to initialize the token number when object creation
	Token(int tokenNumber) {
		this.tokenNumber = tokenNumber;
	}

	// Overriding the toString() to print the details instead of hexadecimal address
	@Override
	public String toString() {
		return "Token [tokenNumber=" + tokenNumber + "]";
	}

}
