package studentexample;

public class EligibilityChecker {

	void printEligibilityToVote(Student s) {

		if (s.age > 18) {
			System.out.println("Eligible to vote");
		} else {
			System.out.println("Not eligible to vote");
		}

		// System.out.println( s.age > 18 ? "Eligible to vote" : "Not eligible to vote");
	}

	public static void main(String[] args) {
		EligibilityChecker e = new EligibilityChecker();

		Student st = new Student("Alpha", 23, 94847474748L);
		e.printEligibilityToVote(st);
	}
}
