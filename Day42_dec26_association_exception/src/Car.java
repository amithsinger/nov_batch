
public class Car {
	
	
	{
		color = "Blue";
		System.out.println("Car is being created");
	}

	String color;
	double price;

	Car(String color, double price) {
		System.out.println("Constructor execution");
		this.color = color;
		this.price = price;
	}

	public static void main(String[] args) {
		Car c = new Car("Blue", 560000);
	}

}
