package com.trai.simexample;

public class JioSim extends AbstractSim {

	@Override
	void call() {
		System.out.println("JIO - Calling...");
	}

	@Override
	void sms() {
		System.out.println("JIO - sending sms...");
	}

	void jioTV() {
		System.out.println("JIO TV - screening movie");
	}

}
