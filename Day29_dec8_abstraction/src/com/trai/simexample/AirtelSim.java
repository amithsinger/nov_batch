package com.trai.simexample;

public class AirtelSim extends AbstractSim {

	@Override
	void call() {
		System.out.println("Airtel - Calling...");
	}

	@Override
	void sms() {
		System.out.println("Airtel - Sending sms...");
	}

}
