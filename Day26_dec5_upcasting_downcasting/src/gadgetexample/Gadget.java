package gadgetexample;

public class Gadget {
	void turnOn() {
		System.out.println("Turning on the gadget");
	}

	void turnOff() {
		System.out.println("Turning off the gadget");
	}
}
