package com.arrayexample.carexample;

import java.util.Arrays;

public class Car {
	String regNo;
	String color;

	Car(String regNo, String color) {
		this.regNo = regNo;
		this.color = color;
	}

	@Override
	public String toString() {
		return "Car [regNo=" + regNo + ", color=" + color + "]";
	}

	public static void main(String[] args) {
		Car c1 = new Car("KA05JF6454", "Blue");
		Car c2 = new Car("KA03RT5647", "Green");

		// create array of Cars and store c1 and c2 in it
		Car[] cars = { c1, c2 };

		// print using for each
		for (Car car : cars) {
			System.out.println(car);
		}

		// printing using toString() of java.util.Arrays class
		System.out.println(Arrays.toString(cars));
		
		
	}

}
