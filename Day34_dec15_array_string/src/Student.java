import java.util.Arrays;

public class Student {
	int sno;
	String name;
	int age;

	Student(int sno, String name, int age) {
		this.sno = sno;
		this.name = name;
		this.age = age;
	}

	@Override
	public String toString() {
		return "Student [sno=" + sno + ", name=" + name + ", age=" + age + "]";
	}

	public static void main(String[] args) {
		Student s1 = new Student(1, "Raj", 24);
		Student s2 = new Student(2, "Jude", 21);
		Student s3 = new Student(3, "Mike", 25);

		Student[] students = { s1, s2, s3 };
		System.out.println(students.length); // 3
		System.out.println(students); // [LStudent;@1175e2db
		students[0].age = 25;
		System.out.println(s1.age); // 25

		System.out.println(Arrays.toString(students));

		System.out.println("_________________________________________");
		System.out.println("Printing using for each loop");
		for (Student student : students) {
			System.out.println(student);
		}

	}

}
