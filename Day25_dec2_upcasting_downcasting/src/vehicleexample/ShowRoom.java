package vehicleexample;

public class ShowRoom {
	// instance method
	Car sellCar() {
		Car c = new Car();
		c.color = "Black";
		c.brand = "Mercedes";
		c.isAutomatic = true;
		c.price = 5000000;
		return c;
	}

	public static void main(String[] args) {
		ShowRoom s = new ShowRoom();
		Car myCar = s.sellCar();
		System.out.println(myCar);
	}
}
