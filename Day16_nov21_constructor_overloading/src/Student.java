
public class Student {
	int sno;
	String name;
	long phoneNo;
	int age;
	String course;

	// ALT + SHIFT + S + O(Owe) -> constructor using fields
	Student(int sno, String name, long phoneNo) {
		this.sno = sno;
		this.name = name;
		this.phoneNo = phoneNo;
	}

	Student(int sno, String name, long phoneNo, int age, String course) {
		this.sno = sno;
		this.name = name;
		this.phoneNo = phoneNo;
		this.age = age;
		this.course = course;
	}

	// ALT + Shift + S + S -> overriding toString()
	@Override
	public String toString() {
		return "Student [sno=" + sno + ", name=" + name + ", phoneNo=" + phoneNo + ", age=" + age + ", course=" + course
				+ "]";
	}

	public static void main(String[] args) {
		Student s = new Student(1, "Alpha", 9373635537L);
		System.out.println(s);
		s.phoneNo = 967564534534L;
		System.out.println(s);
	}

}
