// Converting from higher primitive datatype to lower primitive datatype
// at the time of conversion there will be a type mismatch error.
// we will have to explicitly cast to the lower datatype
public class Narrowing {

	public static void main(String[] args) {
		long l = 130;

		int i = (int) l;

		short s = (short) i;

		byte b = (byte) s;

		System.out.println(l + " " + i + " " + s + " " + b);
	}

}
