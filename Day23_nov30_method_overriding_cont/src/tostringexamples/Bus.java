package tostringexamples;

public class Bus {
	String serviceProvider;
	int seatingCapacity;
	String source;
	String destination;
	double ticketCost;

	// ALT + SHIFT + S + O(Owe) -> Generate constructor using fields
	Bus(String serviceProvider, int seatingCapacity, String source, String destination, double ticketCost) {
		this.serviceProvider = serviceProvider;
		this.seatingCapacity = seatingCapacity;
		this.source = source;
		this.destination = destination;
		this.ticketCost = ticketCost;
	}

	// please override the toString() in such a way it print "Bus to
	// <<DESTINATION>>"
	@Override
	public String toString() {
		return "Bus to " + destination;
	}

	public static void main(String[] args) {
		Bus b = new Bus("Orange", 30, "Bangalore", "Goa", 4000);
		System.out.println(b); // Bus to Goa

		Bus b2 = new Bus("VRL", 34, "Bangalore", "Mangalore", 800);
		System.out.println(b2); // Bus to Mangalore
	}

}
