import java.util.Arrays;
import java.util.Collections;
import java.util.TreeSet;
// ctrl  + shift + O (Owe) is for importing

public class SampleTreeSet {
	public static void main(String[] args) {
		TreeSet<Integer> names = new TreeSet<>(Collections.reverseOrder());
		names.addAll(Arrays.asList(1, 2, 3, 4, 2, 3, 4, 1));
		System.out.println(names);
	}
}
